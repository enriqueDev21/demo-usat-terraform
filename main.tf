resource "aws_instance" "ec2_instance_demo" {
	ami = "ami-09cd431658b5ab3be"
	instance_type = "t2.micro"

	user_data = <<-EOF
		      #!/bin/bash
              apt-get update
              apt-get install apache2 -y
              systemctl enable apache2
              systemctl start apache2
		    EOF

	vpc_security_group_ids = [aws_security_group.sg_demo.id]
}
 
resource "aws_security_group" "sg_demo" {
	name = "security-group-demo"
 
	# ingress {
	# 	from_port = 80
	# 	to_port = 80
	# 	protocol = "tcp"
	# 	cidr_blocks = ["0.0.0.0/0"]
	# }

	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["179.6.82.179/32"]
	}
 
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}