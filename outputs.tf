output "ec2_public_ip" {
  description = "The public IP address for EC2 instance"
  value       = aws_instance.ec2_instance_demo.public_ip
}