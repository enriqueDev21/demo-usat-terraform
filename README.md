# **Demo USAT Terraform**

![terraform](./docs/img-terraform.png)


## **Uso**

Los comandos necesarios para esta demo serán los siguientes

```bash
Validar la version de terraform
> terraform version

Inicializar una configuración de Terraform
> terraform init

Crear plan de ejecución de Terraform
> terraform plan

Ejecutar acciones del plan de ejecución de Terraform
> terraform apply

Destruir la infraestructura gestionada por Terraform
> terraform destroy
```

## **Recursos**

* https://www.whatismyip.com/es/
* https://www.terraform.io/downloads
* https://aws.amazon.com/es/ec2/instance-types/
* https://registry.terraform.io/browse/providers
* https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
* https://www.redhat.com/es/topics/cloud-computing/iaas-vs-paas-vs-saas